#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QStateMachine>
#include <QHistoryState>
#include <QFileDialog>
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    auto stateMachine = new QStateMachine(this);

    auto stateUnlocked = new QState(stateMachine);
    stateUnlocked->assignProperty(ui->pbToggle, "text", "Lock");
    stateUnlocked->assignProperty(ui->pbOpen, "enabled", true);
    stateUnlocked->assignProperty(ui->pbSave, "enabled", true);
    stateUnlocked->assignProperty(ui->teText, "enabled", true);

        auto stateStartup = new QState(stateUnlocked);
        stateStartup->assignProperty(ui->pbOpen, "enabled", true);
        stateStartup->assignProperty(ui->pbSave, "enabled", false);
        stateStartup->assignProperty(ui->teText, "enabled", false);
        stateStartup->assignProperty(ui->teText, "placeholderText", "Open file to start editing...");

        auto stateOpen = new QState(stateUnlocked);
        connect(stateOpen, SIGNAL(entered()), this, SLOT(open()));

        auto stateView = new QState(stateUnlocked);
        stateView->assignProperty(ui->pbOpen, "enabled", true);
        stateView->assignProperty(ui->pbSave, "enabled", false);
        stateView->assignProperty(ui->teText, "enabled", true);

        auto stateEdit = new QState(stateUnlocked);
        stateEdit->assignProperty(ui->pbOpen, "enabled", false);
        stateEdit->assignProperty(ui->pbSave, "enabled", true);

        auto stateSave = new QState(stateUnlocked);
        connect(stateSave, SIGNAL(entered()), this, SLOT(save()));

        auto stateError = new QState(stateUnlocked);
        stateError->assignProperty(ui->pbOpen, "enabled", true);
        stateError->assignProperty(ui->pbSave, "enabled", false);
        stateError->assignProperty(ui->teText, "enabled", false);
        stateStartup->assignProperty(ui->teText, "placeholderText", "Error ocured. Open file to start editing...");

        auto stateHistory = new QHistoryState(stateUnlocked);
        stateHistory->setDefaultState(stateStartup);

        stateStartup->addTransition(ui->pbOpen, SIGNAL(clicked(bool)), stateOpen);
        stateOpen->addTransition(this, SIGNAL(opened()), stateView);
        stateView->addTransition(ui->pbOpen, SIGNAL(clicked(bool)), stateOpen);
        stateOpen->addTransition(this, SIGNAL(error()), stateError);
        stateError->addTransition(ui->pbOpen, SIGNAL(clicked(bool)), stateOpen);
        stateView->addTransition(ui->teText, SIGNAL(textChanged()), stateEdit);
        stateEdit->addTransition(ui->pbSave, SIGNAL(clicked(bool)), stateSave);
        stateSave->addTransition(this, SIGNAL(saved()), stateView);

        stateUnlocked->setInitialState(stateStartup);

    auto stateLocked = new QState(stateMachine);
    stateLocked->assignProperty(ui->pbToggle, "text", "Unlock");
    stateLocked->assignProperty(ui->pbOpen, "enabled", false);
    stateLocked->assignProperty(ui->pbSave, "enabled", false);
    stateLocked->assignProperty(ui->teText, "enabled", false);

    stateUnlocked->addTransition(ui->pbToggle, SIGNAL(clicked(bool)), stateLocked);
    stateLocked->addTransition(ui->pbToggle, SIGNAL(clicked(bool)), stateHistory);
    stateMachine->setInitialState(stateUnlocked);
    stateMachine->start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::open()
{
    fileName = QFileDialog::getOpenFileName(this,
        tr("Open File"), "",
        tr("All Files (*)"));
    if (fileName.isEmpty())
            emit error();
    else
    {
        QFile file(fileName);
        if (!file.open(QIODevice::ReadOnly))
            emit error();
        QTextStream in(&file);
        this->ui->teText->setText(in.readAll());
        file.close();
        emit opened();
    }
}

void MainWindow::save()
{
    QFile file(fileName);
    if (!file.open(QIODevice::WriteOnly))
        emit error();
    QTextStream out(&file);
    out << this->ui->teText->toPlainText();
    file.close();
    emit saved();
}
