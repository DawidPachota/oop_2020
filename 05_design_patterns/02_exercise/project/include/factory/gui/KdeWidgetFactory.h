#ifndef KDEWIDGETFACTORY_H
#define KDEWIDGETFACTORY_H

#include "factory/gui/WidgetFactory.h"

namespace factory::gui {
    class KdeWidgetFactory : public WidgetFactory {
    private:
        int kdeVersion;
    public:
        explicit KdeWidgetFactory(int kdeVersion);
        std::shared_ptr<Widget> create(std::string type);
    };
}
#endif
