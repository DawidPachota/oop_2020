#ifndef CHECKBOXGNOMEWIDGET_H
#define CHECKBOXGNOMEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory::gui {
    class CheckBoxGnomeWidget : public Widget {
    private:
        int gnomeVersion;
    public:
        explicit CheckBoxGnomeWidget(int gnomeVersion);
        void draw() override;
    };


}
#endif
