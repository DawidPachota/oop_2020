#ifndef CHECKBOXKDEWIDGET_H
#define CHECKBOXKDEWIDGET_H

#include "factory/gui/Widget.h"

namespace factory::gui {
    class CheckBoxKdeWidget : public Widget {
    private:
        int kdeVersion;
    public:
        explicit CheckBoxKdeWidget(int kdeVersion);
        void draw() override;
    };


}

#endif
