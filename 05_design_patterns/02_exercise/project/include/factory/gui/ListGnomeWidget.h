#ifndef LISTGNOMEWIDGET_H
#define LISTGNOMEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory::gui {
    class ListGnomeWidget : public Widget {
    private:
        int gnomeVersion;
    public:
        explicit ListGnomeWidget(int gnomeVersion);
        void draw() override;
    };
}
#endif
