#ifndef WIDGET_H
#define WIDGET_H

namespace factory::gui {
    class Widget {
    public:
        virtual ~Widget() = default;
        virtual void draw() = 0;
    };
}

#endif
