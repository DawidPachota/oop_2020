#ifndef BUTTONGNOMEWIDGET_H
#define BUTTONGNOMEWIDGET_H
#include "factory/gui/Widget.h"
namespace factory::gui {
    class ButtonGnomeWidget : public Widget {
    private:
        int gnomeVersion;
    public:
        explicit ButtonGnomeWidget(int gnomeVersion);
        void draw() override;
    };
}
#endif
