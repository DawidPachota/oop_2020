#ifndef GNOMEWIDGETFACTORY_H
#define GNOMEWIDGETFACTORY_H
#include "factory/gui/WidgetFactory.h"

namespace factory::gui {
    class GnomeWidgetFactory : public WidgetFactory {
    private:
        int gnomeVersion;
    public:
        explicit GnomeWidgetFactory(int gnomeVersion);
        std::shared_ptr<Widget> create(std::string type);
    };
}
#endif
