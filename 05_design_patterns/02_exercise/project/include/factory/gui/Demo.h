#ifndef DEMO_H
#define DEMO_H
#include "factory/gui/WidgetFactory.h"

namespace factory {
    namespace gui {

        class Demo {
        private:
            std::shared_ptr<WidgetFactory> factory;
        public:
            explicit Demo(std::shared_ptr<WidgetFactory> factory);
            void run(std::string type);

        };
    }
}

#endif
