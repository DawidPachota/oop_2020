#ifndef BUTTONKDEWIDGET_H
#define BUTTONKDEWIDGET_H

#include "factory/gui/Widget.h"
namespace factory::gui {
    class ButtonKdeWidget : public Widget {
    private:
        int kdeVersion;
    public:
        explicit ButtonKdeWidget(int kdeVersion);
        void draw() override;
    };



}
#endif
