#ifndef WIDGETFACTORY_H
#define WIDGETFACTORY_H
#include "factory/gui/Widget.h"
#include <memory>
#include <iostream>

namespace factory::gui {
    class WidgetFactory {
    public:
        virtual std::shared_ptr<Widget> create (std::string type) = 0;
        virtual ~WidgetFactory() = default;
    };
}
#endif
