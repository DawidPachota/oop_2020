#ifndef LISTKDEWIDGET_H
#define LISTKDEWIDGET_H
#include "factory/gui/Widget.h"

namespace factory::gui {
    class ListKdeWidget : public Widget {
    private:
        int kdeVersion;
    public:
        explicit ListKdeWidget(int kdeVersion);
        void draw() override;
    };



}
#endif
