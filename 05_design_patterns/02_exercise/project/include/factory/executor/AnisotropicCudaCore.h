#ifndef ANISOTROPICCUDACORE_H
#define ANISOTROPICCUDACORE_H

#include "factory/executor/Core.h"

namespace factory::executor {
    class AnisotropicCudaCore : public Core {
    private:
        int gpuId;
    public:
        explicit AnisotropicCudaCore(int gpuId);
        void execute() override;
    };
}

#endif
