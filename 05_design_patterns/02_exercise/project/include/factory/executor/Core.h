#ifndef CORE_H
#define CORE_H

namespace factory::executor {
        class Core {
        public:
            virtual ~Core() = default;
            virtual void execute() {

            }
        };
    }
#endif
