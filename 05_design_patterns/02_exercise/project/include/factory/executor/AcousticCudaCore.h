#ifndef ACOUSTICCUDACORE_H
#define ACOUSTICCUDACORE_H

#include "factory/executor/Core.h"

namespace factory::executor {
    class AcousticCudaCore : public Core {
    private:
        int gpuId;
    public:
        explicit AcousticCudaCore(int gpuId);
        void execute() override;
    };
}

#endif
