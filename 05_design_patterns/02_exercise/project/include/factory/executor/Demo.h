#ifndef DEMO_H
#define DEMO_H

#include "factory/executor/CoreFactory.h"

namespace factory::executor{
    class Demo {
    private:
        std::shared_ptr<CoreFactory> factory;
    public:
        explicit Demo(std::shared_ptr<CoreFactory>factory);
        void run (std::string equation);
    };
}
#endif
