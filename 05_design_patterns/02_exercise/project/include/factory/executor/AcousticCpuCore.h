#ifndef ACOUSTICCPUCORE_H
#define ACOUSTICCPUCORE_H
#include "factory/executor/Core.h"

namespace factory::executor {
    class AcousticCpuCore : public Core {
    private:
        int threads;

    public:
        explicit AcousticCpuCore(int threads);
        void execute() override;
    };
}
#endif
