#ifndef ELASTICCUDACORE_H
#define ELASTICCUDACORE_H

#include "factory/executor/Core.h"

namespace factory::executor {
    class ElasticCudaCore : public Core {
    private:
        int gpuId;
    public:
        explicit ElasticCudaCore(int gpuId);
        void execute() override;
    };

}

#endif
