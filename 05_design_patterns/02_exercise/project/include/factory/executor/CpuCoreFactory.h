#ifndef CPUCOREFACTORY_H
#define CPUCOREFACTORY_H

#include "factory/executor/CoreFactory.h"

namespace factory::executor {
    class CpuCoreFactory : public CoreFactory {
    private:
        int threads;
    public:
        explicit CpuCoreFactory(int threads);
        std::shared_ptr<Core> create(std::string equation) override;

    };
}

#endif
