#ifndef CUDACOREFACTORY_H
#define CUDACOREFACTORY_H

#include "factory/executor/CoreFactory.h"

namespace factory::executor {
    class CudaCoreFactory : public CoreFactory {
    private:
        int gpuId;
    public:
        explicit CudaCoreFactory(int gpuId);
        std::shared_ptr<Core> create(std::string equation) override;

    };
}

#endif
