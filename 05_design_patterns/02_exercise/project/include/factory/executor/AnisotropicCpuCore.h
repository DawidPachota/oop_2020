#ifndef ANISOTROPICCPUCORE_H
#define ANISOTROPICCPUCORE_H

#include "factory/executor/Core.h"

namespace factory::executor {
    class AnisotropicCpuCore : public Core {
    private:
        int threads;
    public:
        explicit AnisotropicCpuCore(int threads);
        void execute() override;
    };
}
#endif
