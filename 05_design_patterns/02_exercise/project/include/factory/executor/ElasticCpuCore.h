#ifndef ELASTICCPUCORE_H
#define ELASTICCPUCORE_H
#include "factory/executor/Core.h"

namespace factory::executor {
    class ElasticCpuCore : public Core {
    private:
        int threads;
    public:
        explicit ElasticCpuCore(int threads);
        void execute() override;
    };
}
#endif
