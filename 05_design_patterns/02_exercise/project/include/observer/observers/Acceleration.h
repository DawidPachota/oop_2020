#ifndef ACCELERATION_H
#define ACCELERATION_H

#include "observer/observers/Observer.h"

namespace observer::observers {
        class Acceleration : public Observer {

        private:
            bool hasFirstValue = false;
            bool hasSecondValue = false;
            bool hasThirdValue = false;
            float firstValue = 0;
            float secondValue = 0;
            float thirdValue = 0;

        public:
            float get();

            void notify(float d) override;

            float getFirstValue() const;

            float getSecondValue() const;

            float getThirdValue() const;
        };
    }

#endif
