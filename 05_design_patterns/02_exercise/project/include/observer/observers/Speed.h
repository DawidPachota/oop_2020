#ifndef SPEED_H
#define SPEED_H

#include "observer/observers/Observer.h"

namespace observer::observers{
        class Speed : public Observer{
        private:
            bool hasFirstValue = false;
            bool hasSecondValue = false;
            float firstValue = 0;
            float secondValue = 0;
        public:
            float get();

            void notify(float d);

        };
    }
#endif
