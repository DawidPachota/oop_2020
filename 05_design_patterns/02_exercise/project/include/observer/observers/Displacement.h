#ifndef DISPLACEMENT_H
#define DISPLACEMENT_H

#include "observer/observers/Observable.h"

namespace observer::observers {
        class Displacement : public Observable {
        private:
            float value = 0;

        public:
            void set(float val);
            float getValue() const;
        };
    }

#endif