#ifndef OBSERVER_H
#define OBSERVER_H
namespace observer::observers{
        class Observer{

        public:
            virtual ~Observer() = default;
            virtual void notify(float){};

        };
    }


#endif