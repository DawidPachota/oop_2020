#include <factory/executor/ElasticCudaCore.h>
#include <iostream>

factory::executor::ElasticCudaCore::ElasticCudaCore(int gpuId) : gpuId(gpuId) {

}

void factory::executor::ElasticCudaCore::execute() {
    std::cout<<"factory::executor::ElasticCudaCore{gpuId: " << this->gpuId << "}" << std::endl;
}

