#include <factory/gui/ButtonKdeWidget.h>
#include <iostream>
factory::gui::ButtonKdeWidget::ButtonKdeWidget(int kdeVersion) : kdeVersion(kdeVersion) {

}

void factory::gui::ButtonKdeWidget::draw() {
    std::cout<<"factory::gui::ButtonKdeWidget{kdeVersion: " << this->kdeVersion <<"}" <<std::endl;
}

