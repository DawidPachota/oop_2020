#include <observer/observers/Acceleration.h>

float observer::observers::Acceleration::get() {
    if(hasFirstValue && hasSecondValue && hasThirdValue) {
        return (this->thirdValue - this->secondValue) - (this->secondValue - this->firstValue);
    }
    return 0;
}
void observer::observers::Acceleration::notify(float d) {
    if(hasFirstValue) {
        if(hasSecondValue) {
            if(hasThirdValue) {
                firstValue = secondValue;
                secondValue = thirdValue;
                thirdValue = d;
            }
            hasThirdValue = true;
            thirdValue = d;

        } else {
            hasSecondValue = true;
            secondValue = d;
        }
    } else {
        hasFirstValue = true;
        firstValue = d;
    }

}

