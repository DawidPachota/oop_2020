#include <observer/observers/Speed.h>

float observer::observers::Speed::get() {
    if (this->hasFirstValue && this->hasSecondValue)
        return this->secondValue - this->firstValue;
    return 0;
}

void observer::observers::Speed::notify(float d) {
    if (hasFirstValue) {
        if (hasSecondValue) {
            firstValue = secondValue;
            secondValue = d;
        } else {
            hasSecondValue = true;
            secondValue = d;
        }

    } else {
        firstValue = d;
        hasFirstValue = true;
    }
}

