#ifndef SPACEMOCK_H
#define SPACEMOCK_H

#include "TestIncludes.h"
#include "Space.h"

class SpaceMock : public Space {
public:
    MOCK_METHOD1(create, void(unsigned int dimensions));
    MOCK_CONST_METHOD0(dimensions, unsigned int());
};

#endif
