#include "Universe.h"
#include "TestIncludes.h"
#include "ObserverMock.h"
#include "TimeMock.h"
#include "SpaceMock.h"

TEST(Universe, Create)
{
    StrictMock<SpaceMock> space{};
    EXPECT_CALL(space, create(11)).Times(AtLeast(1));
    EXPECT_CALL(space, dimensions()).WillOnce(Return(0));

    StrictMock<TimeMock> time{};
    EXPECT_CALL(time, create(true)).Times(AtLeast(1));

    StrictMock<ObserverMock> observer{};
    EXPECT_CALL(observer, remember("How many dimensions there are?", std::to_string(0))).Times(AtLeast(1));

    Universe universe{time, space, observer};
    universe.create();
}

TEST(Universe, Earth) {
    StrictMock<SpaceMock> space{};

    StrictMock<TimeMock> time{};
    EXPECT_CALL(time, now()).WillOnce(Return(9300000000)).WillOnce(Return(9300000001));
    EXPECT_CALL(time, flow()).Times(1);

    StrictMock<ObserverMock> observer{};
    EXPECT_CALL(observer, remember("Is there planet Earth?", "Yes!")).Times(1);

    Universe universe{time, space, observer};
    universe.simulate(9300000001);
}

TEST(Universe, Life) {
    StrictMock<TimeMock> time{};
    EXPECT_CALL(time, now()).WillOnce(Return(9900000000)).WillOnce(Return(9900000001));
    EXPECT_CALL(time, flow()).Times(1);

    StrictMock<SpaceMock> space{};

    StrictMock<ObserverMock> observer{};
    EXPECT_CALL(observer,remember("Does life exist?", "Yes!")).Times(1);

    Universe universe(time, space, observer);
    universe.simulate(9900000001);
}

TEST(Universe, PeopleEvolution) {
    StrictMock<TimeMock> time{};
    EXPECT_CALL(time, now()).WillOnce(Return(13800000000)).WillOnce(Return(13800000001));
    EXPECT_CALL(time, flow()).Times(1);

    StrictMock<SpaceMock> space{};

    StrictMock<ObserverMock> observer{};
    EXPECT_CALL(observer, remember("Have People evolved?", "Yes!")).Times(1);

    Universe universe(time, space, observer);
    universe.simulate(13800000001);
}