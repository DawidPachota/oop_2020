#ifndef TIME_H
#define TIME_H

class Time {
public:
    virtual ~Time() = default;
    virtual void create(bool endless) = 0;
    virtual void flow() = 0;
    [[nodiscard]] virtual long long int now() const = 0 ;

private:
    long long year;
    bool endless;
};

#endif
