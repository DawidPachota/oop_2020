#ifndef OBSERVER_H
#define OBSERVER_H

#include <map>
#include <string>

class Observer {
public:
    virtual ~Observer() = default;
    virtual void remember(std::string question, std::string answer) = 0;
    [[nodiscard]] virtual std::string answer(std::string question) const = 0;

};

#endif
