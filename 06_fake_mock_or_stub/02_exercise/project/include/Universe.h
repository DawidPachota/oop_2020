#ifndef UNIVERSE_H
#define UNIVERSE_H

#include "Time.h"
#include "Space.h"
#include "Observer.h"

class Universe {
public:

    explicit Universe(Time &time, Space &space, Observer &observer);

    void create();
    void simulate(long years);

private:
    Time &time;
    Space &space;
    Observer &observer;
};

#endif