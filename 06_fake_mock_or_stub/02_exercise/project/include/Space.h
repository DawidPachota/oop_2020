#ifndef SPACE_H
#define SPACE_H

class Space {

public:
    virtual ~Space() = default;
    virtual void create(unsigned int dimensions) = 0;
    [[nodiscard]] virtual unsigned int dimensions() const = 0;

};

#endif
