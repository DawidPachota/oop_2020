#ifndef TIMEIMPL_H
#define TIMEIMPL_H

#include "Time.h"

class TimeImpl : public Time {
public:

    void create(bool end_less) override;
    [[nodiscard]] long long now() const override;
    void flow() override;

private:
    long long year;
    bool endless;
};

#endif