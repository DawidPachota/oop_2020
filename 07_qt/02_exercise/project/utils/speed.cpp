#include "speed.h"

Speed::Speed(QObject *parent) : QObject(parent){}

float Speed::get() {
    if (this->hasFirstValue && this->hasSecondValue)
        return this->secondValue - this->firstValue;
    return 0;
}

void Speed::notify(float d) {
    if (hasFirstValue) {
        if (hasSecondValue) {
            firstValue = secondValue;
            secondValue = d;
        } else {
            hasSecondValue = true;
            secondValue = d;
        }

    } else {
        firstValue = d;
        hasFirstValue = true;
    }
}
