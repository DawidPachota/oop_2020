#include "acceleration.h"

Acceleration::Acceleration(QObject *parent) : QObject(parent){}

float Acceleration::get() {
    if(hasFirstValue && hasSecondValue && hasThirdValue) {
        return (this->thirdValue - this->secondValue) - (this->secondValue - this->firstValue);
    }
    return 0;
}
void Acceleration::notify(float d) {
    if(hasFirstValue) {
        if(hasSecondValue) {
            if(hasThirdValue) {
                firstValue = secondValue;
                secondValue = thirdValue;
                thirdValue = d;
            }
            hasThirdValue = true;
            thirdValue = d;

        } else {
            hasSecondValue = true;
            secondValue = d;
        }
    } else {
        hasFirstValue = true;
        firstValue = d;
    }

}
