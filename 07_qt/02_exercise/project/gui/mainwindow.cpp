#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    Speed::connect(&displacement, SIGNAL(valueChanged(float)), &speed, SLOT(notify(float)));
    Acceleration::connect(&displacement, SIGNAL(valueChanged(float)), &acceleration, SLOT(notify(float)));
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()
{

    this->displacement.set(ui->doubleSpinBox->value());
    ui->lcdNumberSpeed->display(this->speed.get());
    ui->lcdNumberAcceleration->display(this->acceleration.get());
}
