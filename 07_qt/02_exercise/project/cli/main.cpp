#include <QCoreApplication>

#include "utils.h"
#include "speed.h"
#include "displacement.h"
#include "acceleration.h"
#include <QtDebug>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    Speed speed{};
    Displacement displacement{};
    Acceleration acceleration{};

    Speed::connect(&displacement, SIGNAL(valueChanged(float)), &speed, SLOT(notify(float)));
    Acceleration::connect(&displacement, SIGNAL(valueChanged(float)), &acceleration, SLOT(notify(float)));

    displacement.set(2);
    displacement.set(10);
    qDebug()<<speed.get()<<" "<<acceleration.get();


    return a.exec();
}
