#include <deque>
#include <iostream>
#include <unordered_map>
#include "Dummy.h"
#include "Small.h"
#include "BenchIncludes.h"

static void Dummy_Integrate(State& state) {

    Dummy dummy{0, state.range(0)};

    for (auto _ : state) {


        auto integral = dummy.integrate();

        DoNotOptimize(integral);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(Dummy_Integrate)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeAt(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque.at(rand()%N);
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeAt)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeBrackets(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque[rand()%N];
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeBrackets)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeFront(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque.front();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeFront)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeBack(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque.back();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeBack)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeEmpty(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque.empty();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeEmpty)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeSize(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque.size();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeSize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeMaxSize(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        auto result = deque.max_size();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeMaxSize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeShrinkToFit(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        deque.shrink_to_fit();
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeShrinkToFit)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeClear(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {

        deque.clear();
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeClear)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeInsert(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {
        Small small{};
        small.randomize();
        auto it = deque.begin()+rand()%N;
        deque.insert( it, small );
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeInsert)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequePushBack(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequePushBack)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequePopBack(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {
        deque.pop_back();
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequePopBack)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequePushFront(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }


    for (auto _ : state) {
        Small small{};
        small.randomize();
        deque.push_front(small);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequePushFront)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequePopFront(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }

    for (auto _ : state) {
        deque.pop_front();
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequePopFront)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeResize(State& state) {

    std::deque<Small> deque{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
    }

    for (auto _ : state) {
        deque.resize(rand()%N*4);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeResize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void dequeSwap(State& state) {

    std::deque<Small> deque{};
    std::deque<Small> deque2{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        deque.push_back(small);
        Small small2{};
        small2.randomize();
        deque2.push_back(small2);
    }

    for (auto _ : state) {
        deque.swap(deque2);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(dequeSwap)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

//---------multimap--------------

static void multimapEmpty(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto result = multimap.empty();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapEmpty)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapSize(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto result = multimap.size();
        DoNotOptimize(&result);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapSize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapMaxSize(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto result = multimap.max_size();
        DoNotOptimize(&result);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapMaxSize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapClear(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        multimap.clear();

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapClear)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapInsert(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        Small small{};
        small.randomize();
        auto i = rand()%N;
        multimap.insert({i,small});

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapInsert)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapErase(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        multimap.erase(i);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapErase)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapSwap(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};
    std::multimap<int, Small, std::greater<>> multimap2{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
        Small small2{};
        small2.randomize();
        multimap2.insert({i,small2});
    }

    for (auto _ : state) {
        multimap.swap(multimap2);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapSwap)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapCount(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        multimap.count(i);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapCount)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapFind(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        multimap.find(i);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapFind)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapEqualRange(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        multimap.equal_range(i);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapEqualRange)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapLowerBound(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        multimap.lower_bound(i);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapLowerBound)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void multimapUpperBound(State& state) {

    std::multimap<int, Small, std::greater<>> multimap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        multimap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        multimap.upper_bound(i);

    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(multimapUpperBound)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

//---------unorderedMap---------------

static void unorderedMapEmpty(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto result = unorderedMap.empty();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapEmpty)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapSize(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto result = unorderedMap.size();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapSize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapMaxSize(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto result = unorderedMap.max_size();
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapMaxSize)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapClear(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        unorderedMap.clear();
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapClear)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapInsert(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        Small small{};
        small.randomize();
        auto i = rand()%N;
        unorderedMap.insert({i,small});
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapInsert)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapErase(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        unorderedMap.erase(i);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapErase)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapSwap(State& state) {

    std::unordered_map<int, Small> unorderedMap{};
    std::unordered_map<int, Small> unorderedMap2{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
        Small small2{};
        small2.randomize();
        unorderedMap2.insert({i,small2});
    }

    for (auto _ : state) {
        unorderedMap.swap(unorderedMap2);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapSwap)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapAt(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        auto result = unorderedMap.at(i);
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapAt)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapBrackets(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        auto result = unorderedMap[i];
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapBrackets)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapCount(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        auto result = unorderedMap.count(i);
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapCount)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapFind(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        auto result = unorderedMap.find(i);
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapFind)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapEqualRange(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        auto result = unorderedMap.equal_range(i);
        DoNotOptimize(&result);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapEqualRange)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapRehash(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        unorderedMap.rehash(i);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapRehash)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();

static void unorderedMapReserve(State& state) {

    std::unordered_map<int, Small> unorderedMap{};

    auto N = state.range(0);

    for(auto i=0u; i<N; i++) {
        Small small{};
        small.randomize();
        unorderedMap.insert({i,small});
    }

    for (auto _ : state) {
        auto i = rand()%N;
        unorderedMap.reserve(unorderedMap.size() + i);
    }

    state.SetComplexityN(state.range(0));
}

BENCHMARK(unorderedMapReserve)->RangeMultiplier(2)->Range(1u << 5u, 1u << 18u)->Complexity();