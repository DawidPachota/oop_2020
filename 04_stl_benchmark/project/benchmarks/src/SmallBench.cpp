#include "Small.h"
#include "BenchIncludes.h"

static void smallLessThanOperator(State& state) {

    Small a{};
    Small b{};

    a.randomize();
    b.randomize();

    for(auto _: state) {
        auto result = a < b;
        DoNotOptimize(&result);
    }
}

BENCHMARK(smallLessThanOperator);

static void smallEqualOperator(State& state) {

    Small a{};
    Small b{};

    a.randomize();
    b.randomize();

    for(auto _: state) {
        auto result = a == b;
        DoNotOptimize(&result);
    }
}

BENCHMARK(smallEqualOperator);

static void smallHash(State& state) {

    Small a{};

    a.randomize();

    std::hash<Small> hash;

    for(auto _: state) {
        auto result = hash(a);
        DoNotOptimize(&result);
    }
}

BENCHMARK(smallHash);
