#include "Medium.h"
#include "BenchIncludes.h"

static void mediumLessThanOperator(State& state) {

    Medium a{};
    Medium b{};

    a.randomize();
    b.randomize();

    for(auto _: state) {
        auto result = a < b;
        DoNotOptimize(&result);
    }
}

BENCHMARK(mediumLessThanOperator);

static void mediumEqualOperator(State& state) {

    Medium a{};
    Medium b{};

    a.randomize();
    b.randomize();

    for(auto _: state) {
        auto result = a == b;
        DoNotOptimize(&result);
    }
}

BENCHMARK(mediumEqualOperator);

static void mediumHash(State& state) {

    Medium a{};

    a.randomize();

    std::hash<Medium> hash;

    for(auto _: state) {
        auto result = hash(a);
        DoNotOptimize(&result);
    }
}

BENCHMARK(mediumHash);