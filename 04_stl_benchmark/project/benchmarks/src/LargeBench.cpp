#include "Large.h"
#include "BenchIncludes.h"

static void largeLessThanOperator(State& state) {

    Large a{};
    Large b{};

    a.randomize();
    b.randomize();

    for(auto _: state) {
        auto result = a < b;
        DoNotOptimize(&result);
    }
}

BENCHMARK(largeLessThanOperator);

static void largeEqualOperator(State& state) {

    Large a{};
    Large b{};

    a.randomize();
    b.randomize();

    for(auto _: state) {
        auto result = a == b;
        DoNotOptimize(&result);
    }
}

BENCHMARK(largeEqualOperator);

static void largeHash(State& state) {

    Large a{};

    a.randomize();

    std::hash<Large> hash;

    for(auto _: state) {
        auto result = hash(a);
        DoNotOptimize(&result);
    }
}

BENCHMARK(largeHash);
