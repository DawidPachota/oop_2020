#pragma once

#include <cstdlib>

struct Element{
    int value;
    bool isSet;
};

class RingBuffer {
public:

    explicit RingBuffer(int size = 5);

    int getSize() const;

    int getElement(int index) const;

    void addElement(int element);

    void removeElement();

private:
    int size;
    struct Element* tab;
    int addPointer;
    int removePointer;
};