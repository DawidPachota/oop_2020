#pragma once

#include <string>
#include <vector>
#include <sstream>

class TextWrap {
public:

    explicit TextWrap(int columns = 10);

    int getColumns() const;

    std::string wrap(std::string string) const;

private:
    int columns;
};