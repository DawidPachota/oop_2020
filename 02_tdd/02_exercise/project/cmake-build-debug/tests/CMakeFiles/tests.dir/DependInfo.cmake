# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/student/oop_2020/02_tdd/02_exercise/project/tests/src/CalculatorTest.cpp" "/home/student/oop_2020/02_tdd/02_exercise/project/cmake-build-debug/tests/CMakeFiles/tests.dir/src/CalculatorTest.cpp.o"
  "/home/student/oop_2020/02_tdd/02_exercise/project/tests/src/RingBufferTest.cpp" "/home/student/oop_2020/02_tdd/02_exercise/project/cmake-build-debug/tests/CMakeFiles/tests.dir/src/RingBufferTest.cpp.o"
  "/home/student/oop_2020/02_tdd/02_exercise/project/tests/src/TextWrapTest.cpp" "/home/student/oop_2020/02_tdd/02_exercise/project/cmake-build-debug/tests/CMakeFiles/tests.dir/src/TextWrapTest.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../tests/../../googletest/googletest/include"
  "../include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/student/oop_2020/02_tdd/02_exercise/project/cmake-build-debug/CMakeFiles/utils.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
