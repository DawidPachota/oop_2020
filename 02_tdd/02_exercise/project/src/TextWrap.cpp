#include "TextWrap.h"

TextWrap::TextWrap(int columns) : columns(columns) {}

int TextWrap::getColumns() const {
    return columns;
}

std::string TextWrap::wrap(std::string line) const {

    if(line.size() > columns) {
        std::istringstream stream(line);
        std::string word;
        std::string result;
        std::string currentLine;

        while (stream >> word) {
            while(word.size() > columns) {
                if( !currentLine.empty() ) {
                    result += currentLine + '\n';
                    currentLine = "";
                }
                result += word.substr(0, columns) + '\n';
                word = word.substr(columns);
            }
            if( currentLine.empty() ) {
                currentLine = word;
            }
            else if ( (currentLine.size() + word.size() + 1) > columns ){
                result += currentLine +'\n';
                currentLine = word;
            }
            else
                currentLine += ' ' + word;
        }
        result += currentLine;

        return result;
    }

    return line;
}
