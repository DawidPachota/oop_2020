#include "RingBuffer.h"

RingBuffer::RingBuffer(int size) : size(size){
    tab = (Element*) ( size * sizeof(Element) );
    for( int i=0; i<size; i++) {
        tab[i].isSet = false;
        tab[i].value = 0;
    }
    addPointer = 0;
    removePointer = 0;
}

int RingBuffer::getSize() const {
    return size;
}

int RingBuffer::getElement(int index) const {
    if(tab[index].isSet)
        return tab[index].value;
    return NULL;
}

void RingBuffer::addElement(int element){
    tab[addPointer].value = element;
    tab[addPointer].isSet = true;
    if(addPointer + 1 == size )
        addPointer = 0;
    else
        addPointer++;
}

void RingBuffer::removeElement(){
    tab[removePointer].value = 0;
    tab[removePointer].isSet = false;
    removePointer++;
}