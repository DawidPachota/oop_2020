#include "TextWrap.h"
#include <gtest/gtest.h>

TEST(TextWrap, Constructor) {

    TextWrap textWrap{};
}

TEST(TextWrap, Constructor_PassNumberOfColmnsInConstructor) {

    TextWrap textWrap{10};
}

TEST(TextWrap, GetColumns) {

    TextWrap textWrap{10};
    EXPECT_EQ(10, textWrap.getColumns());
}

TEST(TextWrap, GetColumns_OtherNumberOfColumns) {

    TextWrap textWrap{13};
    EXPECT_EQ(13, textWrap.getColumns());
}

TEST(TextWrap, Wrap_EmptyString) {

    TextWrap textWrap{1};
    EXPECT_EQ("", textWrap.wrap(""));
}

TEST(TextWrap, Wrap_OneCharacterInOneColumn) {

    TextWrap textWrap{1};
    EXPECT_EQ("a", textWrap.wrap("a"));
}

TEST(TextWrap, Wrap_TwoCharacterInOneColumn) {

    TextWrap textWrap{1};
    EXPECT_EQ("a\nb", textWrap.wrap("ab"));
}

TEST(TextWrap, Wrap_FourCharacterInTwoColumn) {

    TextWrap textWrap{2};
    EXPECT_EQ("ab\ncd", textWrap.wrap("abcd"));
}

TEST(TextWrap, Wrap_ThreeCharacterInThreeColumn) {

    TextWrap textWrap{1};
    EXPECT_EQ("a\nb\nc", textWrap.wrap("abc"));
}

TEST(TextWrap, Wrap_FiveCharacterInTwoColumn) {

    TextWrap textWrap{2};
    EXPECT_EQ("ab\nbc\nc", textWrap.wrap("abbcc"));
}

TEST(TextWrap, Wrap_TenCharacterInThreeColumn) {

    TextWrap textWrap{3};
    EXPECT_EQ("abc\ndef\nghi\nj", textWrap.wrap("abcdefghij"));
}

TEST(TextWrap, Wrap_SixCharacterInOneColumn) {

    TextWrap textWrap{1};
    EXPECT_EQ("a\nb\nc\nd\ne\nf", textWrap.wrap("abcdef"));
}

TEST(TextWrap, Wrap_TwoWordsInTwoRow) {

    TextWrap textWrap{3};
    EXPECT_EQ("abc\ndef", textWrap.wrap("abc def"));
}

TEST(TextWrap, Wrap_ThreeWordsInThreeRow) {

    TextWrap textWrap{3};
    EXPECT_EQ("abc\ndef\nghi", textWrap.wrap("abc def ghi"));
}

TEST(TextWrap, Wrap_ThreeWordsInTwoRow) {

    TextWrap textWrap{7};
    EXPECT_EQ("abc def\nghi", textWrap.wrap("abc def ghi"));
}

TEST(TextWrap, Wrap_ThreeWordsInThreeRowWithLongerWord) {

    TextWrap textWrap{3};
    EXPECT_EQ("ab\ndef\nghi", textWrap.wrap("ab def ghi"));
}

TEST(TextWrap, Wrap_ThreeWordsInThreeRowWithCuttedWord) {

    TextWrap textWrap{3};
    EXPECT_EQ("a\ndef\nghi", textWrap.wrap("a def ghi"));
}

TEST(TextWrap, Wrap_LongLine) {

    TextWrap textWrap{12};
    EXPECT_EQ("Lorem ipsum\ndolor sit\namet,\nconsectetur\nadipiscing\nelit.", textWrap.wrap("Lorem ipsum dolor sit amet, consectetur adipiscing elit."));
}

TEST(TextWrap, Wrap_LongLineWithCuttedWord) {

    TextWrap textWrap{12};
    EXPECT_EQ("Lorem ipsum\ndolor sit\namet,\nconsecteture\nrer\nadipiscing\nelit.", textWrap.wrap("Lorem ipsum dolor sit amet, consecteturerer adipiscing elit."));
}

TEST(TextWrap, Wrap_LongLongLine) {

    TextWrap textWrap{50};
    EXPECT_EQ("Lorem ipsum dolor sit amet, consectetur adipiscing\nelit. Duis accumsan dignissim ante vel suscipit.\nAenean suscipit ex porttitor, lobortis eros non,\ncursus nunc. In consectetur, magna nec sodales\negestas, nisi felis tincidunt ipsum, et ornare\nmassa magna sit amet eros. Etiam bibendum eros\nviverra augue ultrices vehicula. Etiam risus\nlectus, rhoncus vitae odio eget, sagittis\nmalesuada nisl. Phasellus accumsan mi lorem, eget\nfinibus purus pulvinar nec. Aliquam consequat\nligula et maximus lobortis. In volutpat libero\nvitae eros gravida aliquet. Interdum et malesuada\nfames ac ante ipsum primis in faucibus. Fusce ac\nfelis sapien. Nulla at lacus non risus imperdiet\nbibendum sit amet molestie purus. Nunc quis rutrum\nest. Sed id nisl non tortor facilisis rhoncus.",
              textWrap.wrap("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis accumsan dignissim ante vel suscipit. Aenean suscipit ex porttitor, lobortis eros non, cursus nunc. In consectetur, magna nec sodales egestas, nisi felis tincidunt ipsum, et ornare massa magna sit amet eros. Etiam bibendum eros viverra augue ultrices vehicula. Etiam risus lectus, rhoncus vitae odio eget, sagittis malesuada nisl. Phasellus accumsan mi lorem, eget finibus purus pulvinar nec. Aliquam consequat ligula et maximus lobortis. In volutpat libero vitae eros gravida aliquet. Interdum et malesuada fames ac ante ipsum primis in faucibus. Fusce ac felis sapien. Nulla at lacus non risus imperdiet bibendum sit amet molestie purus. Nunc quis rutrum est. Sed id nisl non tortor facilisis rhoncus."));
}