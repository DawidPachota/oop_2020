#include "RingBuffer.h"
#include <gtest/gtest.h>

TEST(RingBuffer, Constructor) {

    RingBuffer ringBuffer{};
}

TEST(RingBuffer, Constructor_PassSizeInConstructor) {

    RingBuffer ringBuffer{10};
}

TEST(RingBuffer, GetSize) {

    RingBuffer ringBuffer{10};
    EXPECT_EQ(10, ringBuffer.getSize());
}

TEST(RingBuffer, GetSizeWithDefault) {

    RingBuffer ringBuffer{};
    EXPECT_EQ(5, ringBuffer.getSize());
}

TEST(RingBuffer, GetTabValues) {

    RingBuffer ringBuffer{};
    EXPECT_EQ(0, (ringBuffer.getElement(0)) );
    EXPECT_EQ(0, (ringBuffer.getElement(1)) );
    EXPECT_EQ(0, (ringBuffer.getElement(2)) );
    EXPECT_EQ(0, (ringBuffer.getElement(3)) );
    EXPECT_EQ(0, (ringBuffer.getElement(4)) );
}

TEST(RingBuffer, AddElement) {

    RingBuffer ringBuffer{};
    ringBuffer.addElement(1);
    EXPECT_EQ(1, (ringBuffer.getElement(0)) );
}

TEST(RingBuffer, AddTwoElements) {

    RingBuffer ringBuffer{};
    ringBuffer.addElement(1);
    ringBuffer.addElement(2);
    EXPECT_EQ(1, (ringBuffer.getElement(0)) );
    EXPECT_EQ(2, (ringBuffer.getElement(0)) );
}

TEST(RingBuffer, AddThreeElementsWithSizeTwo) {

    RingBuffer ringBuffer{2};
    ringBuffer.addElement(1);
    ringBuffer.addElement(2);
    ringBuffer.addElement(3);
    EXPECT_EQ(3, (ringBuffer.getElement(0)) );
    EXPECT_EQ(2, (ringBuffer.getElement(1)) );
}

TEST(RingBuffer, AddFiveElementsWithSizeTwo) {

    RingBuffer ringBuffer{2};
    ringBuffer.addElement(1);
    ringBuffer.addElement(2);
    ringBuffer.addElement(3);
    ringBuffer.addElement(4);
    ringBuffer.addElement(5);
    EXPECT_EQ(5, (ringBuffer.getElement(0)) );
    EXPECT_EQ(4, (ringBuffer.getElement(1)) );
}

TEST(RingBuffer, CheckNulls ) {

    RingBuffer ringBuffer{2};
    EXPECT_EQ( (NULL), (ringBuffer.getElement(0)) );
    EXPECT_EQ( (NULL), (ringBuffer.getElement(0)) );
}

TEST(RingBuffer, RemoveElement ) {

    RingBuffer ringBuffer{2};
    ringBuffer.addElement(1);
    ringBuffer.addElement(2);
    ringBuffer.removeElement();
    EXPECT_EQ((NULL), (ringBuffer.getElement(0)) );
    EXPECT_EQ( 2, (ringBuffer.getElement(1)) );
}

TEST(RingBuffer, RemoveTwoElements ) {

    RingBuffer ringBuffer{2};
    ringBuffer.addElement(1);
    ringBuffer.addElement(2);
    ringBuffer.removeElement();
    ringBuffer.removeElement();
    EXPECT_EQ((NULL), (ringBuffer.getElement(0)) );
    EXPECT_EQ( (NULL), (ringBuffer.getElement(1)) );
}
