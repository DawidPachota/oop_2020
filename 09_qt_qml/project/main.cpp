#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "speed.h"
#include "acceleration.h"
#include "displacement.h"

int main(int argc, char *argv[])
{
#if QT_VERSION < QT_VERSION_CHECK(6, 0, 0)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif

    QGuiApplication app(argc, argv);

    qmlRegisterType<Displacement>("pl.pachota", 1, 0, "Displacement");
    qmlRegisterType<Speed>("pl.pachota", 1, 0, "Speed");
    qmlRegisterType<Acceleration>("pl.pachota", 1, 0, "Acceleration");

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);
    engine.load(url);

    auto displacement = engine.rootObjects().first()->findChild<Displacement*>();
    auto speed = engine.rootObjects().first()->findChild<Speed*>();
    auto acceleration = engine.rootObjects().first()->findChild<Acceleration*>();

    QObject::connect(displacement, SIGNAL(valueChanged(float)),
                     speed, SLOT(notify(float)));
    QObject::connect(displacement, SIGNAL(valueChanged(float)),
                     acceleration, SLOT(notify(float)));


    return app.exec();
}
