import QtQuick.Window 2.2
import QtQuick 2.9

import QtQuick.Controls 2.2

import pl.pachota 1.0


Window {
    id: window
    width: 856
    height: 588
    color: "#f1f1f1"
    visible: true
    title: qsTr("Acceleration")

    Speed {
        id: speed
        value: speed.get();
    }

    Acceleration {
        id: acceleration
        value: acceleration.get();

    }

    Displacement {
        id: displacement
    }

    Dial {
        id: speedDial
        x: 56
        y: 96
        width: 343
        height: 323
        antialiasing: true
        wheelEnabled: true
        topPadding: 0
        hoverEnabled: false
        enabled: false
        value: speed.value / 100

    }

    Dial {
        id: accelerationDial
        x: 443
        y: 96
        width: 370
        height: 323
        font.bold: false
        font.wordSpacing: 0
        font.weight: Font.Normal
        antialiasing: true
        transformOrigin: Item.Center
        rotation: 0
        z: 0
        wheelEnabled: true
        font.pointSize: 9
        hoverEnabled: false
        enabled: false
        value: acceleration.value / 100

        Text {
            id: element1
            x: 138
            y: 107
            width: 95
            height: 14
            color: "#5500ff"
            text: qsTr("ACCELERATION")
            scale: 2
            font.pixelSize: 12
        }

    }



    Text {
        id: speedIndicator
        x: 162
        y: 241
        width: 131
        height: 33
        color: "#5500ff"
        text: parseInt(speedDial.value * 100)
        scale: 2
        visible: true
        layer.smooth: true
        layer.format: ShaderEffectSource.Alpha
        font.pixelSize: 26
        horizontalAlignment: Text.AlignHCenter
        font.family: "Arial"



    }

    Text {
        id: acceleraionIndicator
        x: 567
        y: 241
        width: 122
        height: 33
        color: "#5500ff"
        text: parseInt(accelerationDial.value * 100)
        layer.textureSize.width: 1
        enabled: false
        scale: 2
        visible: true
        clip: false
        layer.smooth: true
        layer.format: ShaderEffectSource.Alpha
        font.pixelSize: 26
        horizontalAlignment: Text.AlignHCenter
        font.family: "Arial"
    }

    TextField {
        id: displacementText
        x: 366
        y: 17
        width: 77
        height: 26
        validator: IntValidator {bottom: 1; top: 100}
    }

    Button {
        id: button
        x: 443
        y: 17
        width: 52
        height: 26
        text: qsTr("Set")
        font.weight: Font.Medium
        font.family: "Arial"
        onClicked: function() {
            var value = parseFloat(displacementText.text);
            displacement.set(value);
            speedAnimation.running = true;
            accelerationAnimation.running = true;
        }
            NumberAnimation {
            id: speedAnimation
            target: speedDial
            property: "value"
            duration: 1000
            from: 0
            to: speed.value / 100
        }

            NumberAnimation {
            id: accelerationAnimation
            target: accelerationDial
            property: "value"
            duration: 1000
            from: 0
            to: acceleration.value / 100
            }
    }

    Text {
        id: element
        x: 207
        y: 203
        width: 41
        height: 16
        color: "#5500ff"
        text: qsTr("SPEED")
        scale: 2
        font.pixelSize: 12
    }

}


