#include "counter.h"

#include <QDebug>

Counter::Counter(QObject *parent) : QObject(parent), value(0)
{
    qDebug() << "Counter::Counter(QObject *)";
}


void Counter::initisalise(int v)
{
    qDebug() << "void Counter::initisalise(int)";

    value = v;

    emit changed();
}

int Counter::get() const
{
    qDebug() << "int Counter::get() const";

    return value;
}

void Counter::set(int v)
{
    qDebug() << "void Counter::set(int)";

    value = v;

    emit changed();
}

void Counter::increment()
{
    qDebug() << "void Counter::increment()";

    value += 1;

    emit changed();
}
